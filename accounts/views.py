from django.shortcuts import render, redirect
from .forms import RegistrationForm
from .models import User


# Create your views here.

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('registration_success')  # Chuyển hướng khi đăng ký thành công
    else:
        form = RegistrationForm()
    return render(request, 'registration/register.html', {'form': form})

def registration_success(request):
    return render(request, 'registration/success.html')
